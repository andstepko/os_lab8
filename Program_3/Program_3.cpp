// Program_3.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "windows.h"
#include "tlhelp32.h"
using namespace std;

#define CreateUnsuspendedProcess(commandLine, currDirectory, startupInfo, processInfo) CreateProcess(0, commandLine, 0, 0, 0, 0, 0, currDirectory, startupInfo, processInfo);
#define CreateSuspendedProcess(commandLine, currDirectory, startupInfo, processInfo) CreateProcess(0, commandLine, 0, 0, 0, CREATE_SUSPENDED, 0, currDirectory, startupInfo, processInfo);

unsigned __int64 StringToInt64(TCHAR* str)
{
	unsigned __int64 result = 0;
	int i = 0;
	while (str[i] != '\0')
	{
		result = result * 10 + (str[i] - '0');
		i++;
	}
	return result;
}

bool PrintUnicodeFileInfo(HANDLE hFile)
{
	wchar_t wText[64*1024];
	DWORD dwFileSize;
	DWORD dwRead;
	BOOL b;

	// Read the BOM.
	b = ReadFile(hFile, &wText, 2, &dwRead, 0);

	dwFileSize = GetFileSize(hFile, 0);
	b = ReadFile(hFile, &wText, dwFileSize, &dwRead, 0);
	if(!b)
	{
		_tprintf(_T("ERROR reading file!!!\n"));
		return false;
	}
	wText[dwRead / sizeof(wText[0])] = '\0';
	_tprintf(_T("File was successfully read.\n"));

	_tprintf(_T("The size of the file is: "));
	_tprintf(_T("%d"), dwRead + 2);
	_tprintf(_T(" bytes.\n"));

	DWORD stringCount = 0;
	DWORD symbolCount = 0;

	for(int i = 0; i < sizeof(wText) / sizeof(wText[0]); i++)
	{
		if(wText[i] == '\r')
		{
			_tprintf(_T("Line number "));
			_tprintf(_T("%d"), stringCount);
			_tprintf(_T(" : "));
			_tprintf(_T("%d"), symbolCount);
			_tprintf(_T(" symbols.\n"));
			stringCount++;
			symbolCount = 0;
			i++;
		}
		else if(wText[i] == '\0')
		{
			break;
		}
		else
		{
			symbolCount++;
		}
	}

	// Last line.
	_tprintf(_T("Line number "));
	_tprintf(_T("%d"), stringCount);
	_tprintf(_T(" : "));
	_tprintf(_T("%d"), symbolCount);
	_tprintf(_T(" symbols.\n"));

	_tprintf(_T("Total lines : "));
	_tprintf(_T("%d"), stringCount + 1);
	_tprintf(_T("\n"));

	return true;
}

bool PrintAnsiFileInfo(HANDLE hFile)
{
	char text[64*1024];
	DWORD dwFileSize;
	DWORD dwRead;
	BOOL b;

	dwFileSize = GetFileSize(hFile, 0);
	b = ReadFile(hFile, &text, dwFileSize, &dwRead, 0);
	if(!b)
	{
		_tprintf(_T("ERROR reading file!!!\n"));
		return false;
	}
	text[dwRead / sizeof(text[0])] = '\0';
	_tprintf(_T("File was successfully read.\n"));

	_tprintf(_T("The size of the file is: "));
	_tprintf(_T("%d"), dwRead);
	_tprintf(_T(" bytes.\n"));

	DWORD stringCount = 0;
	DWORD symbolCount = 0;

	for(int i = 0; i < sizeof(text) / sizeof(text[0]); i++)
	{
		if(text[i] == '\r')
		{
			_tprintf(_T("Line number "));
			_tprintf(_T("%d"), stringCount);
			_tprintf(_T(" : "));
			_tprintf(_T("%d"), symbolCount);
			_tprintf(_T(" symbols.\n"));
			stringCount++;
			symbolCount = 0;
			i++;
		}
		else if(text[i] == '\0')
		{
			break;
		}
		else
		{
			symbolCount++;
		}
	}

	// Last line.
	_tprintf(_T("Line number "));
	_tprintf(_T("%d"), stringCount);
	_tprintf(_T(" : "));
	_tprintf(_T("%d"), symbolCount);
	_tprintf(_T(" symbols.\n"));

	_tprintf(_T("Total lines : "));
	_tprintf(_T("%d"), stringCount + 1);
	_tprintf(_T("\n"));

	return true;
}

bool PrintFileInfo(HANDLE h, WIN32_FIND_DATA FoundFileData)
{
	DWORD dwBOM = 65279;
	HANDLE hFile;
	DWORD dwFileTop = 0;
	DWORD dwRead;
	BOOL b;
	BOOL bIsUnicode;

	_tprintf(_T("\n    "));
	_tprintf(_T("%s\n"), FoundFileData.cFileName);
	_tprintf(_T("\n"));

	hFile = CreateFile((LPCTSTR)FoundFileData.cFileName, GENERIC_READ, FILE_SHARE_READ, 0, OPEN_EXISTING, 0, 0);
	if(hFile == INVALID_HANDLE_VALUE)
	{
		_tprintf(_T("INVALID file got while reading!!!\n"));
	}

	b = ReadFile(hFile, &dwFileTop, 2, &dwRead, 0);
	SetFilePointer(hFile, 0, 0, FILE_BEGIN);
	if(!b)
	{
		_tprintf(_T("ERROR reading first 2 bytes of the file.\n"));
		bIsUnicode = false;
		_tprintf(_T("File is NOT Unicode.\n"));
	}
	else
	{
		_tprintf(_T("Successfully read first 2 bytes of file.\n"));
		_tprintf(_T("dwFileTop = "));
		_tprintf(_T("%d\n"), dwFileTop);

		if(dwFileTop == dwBOM)
		{
			bIsUnicode = true;
			_tprintf(_T("File IS Unicode.\n"));
			b = PrintUnicodeFileInfo(hFile);
		}
		else
		{
			bIsUnicode = false;
			_tprintf(_T("File is NOT Unicode.\n"));
			b = PrintAnsiFileInfo(hFile);
		}
	}

	return true;
}

bool ThirdProgram()
{
	TCHAR timeFile[] = _T("time.stepko");
	WIN32_FIND_DATA FoundFileData;
	HANDLE hFile = INVALID_HANDLE_VALUE;
	BOOL b;
	unsigned __int64 i64MinCreateTime = 0;

	/*hFile = CreateFile((LPCTSTR)timeFile, GENERIC_READ, FILE_SHARE_READ, 0, OPEN_EXISTING, 0, 0);
	if(hFile == INVALID_HANDLE_VALUE)
	{
		_tprintf(_T("ERROR opening TimeFile!!!\n"));
		return false;
	}
	b = ReadFile(hFile, &�64MinCreateTime, sizeof(�64MinCreateTime), &dwRead, 0);
	if(!b)
	{
		_tprintf(_T("ERROR reading TimeFile!!!\n"));
		return false;
	}
	_tprintf(_T("Successfully read: "));
	_tprintf(_T("%d"), dwRead);
	_tprintf(_T(" bytes from timeFile.\n"));*/

	/*b = CloseHandle(hFile);
	if(b)
	{
		_tprintf(_T("TimeFile handler was successfully closed.\n"));
	}
	else
	{
		_tprintf(_T("TimeFile handler was NOT successfully closed.\n"));
	}*/

	TCHAR tcharGotTime[MAX_PATH];
	GetEnvironmentVariable(_T("CreationTime"), tcharGotTime, MAX_PATH);
	i64MinCreateTime = StringToInt64(tcharGotTime);
	_tprintf(_T("Environment variable was successfully got.\n"));

	_tprintf(_T("Got time: "));
	_tprintf(_T("%I64d"), i64MinCreateTime);
	_tprintf(_T(" .\n"));

	hFile = FindFirstFile(	_T("*.txt"),	&FoundFileData);
	_tprintf(_T("Created descriptor for first *.txt file.\n"));

	if (hFile == INVALID_HANDLE_VALUE)
	{
		_tprintf(_T("No files were found!!!\n"));
		return false;
	}

	while(1)
	{
		FILETIME ftFileCreated = FoundFileData.ftCreationTime;

		FileTimeToLocalFileTime(&ftFileCreated, &ftFileCreated);
		unsigned __int64 i64CurFileCreatedTime = ((unsigned __int64)(ftFileCreated.dwHighDateTime)<<32) 
			| ftFileCreated.dwLowDateTime;

		if(i64MinCreateTime < i64CurFileCreatedTime)
		{
			_tprintf(_T("\n"));
			PrintFileInfo(hFile, FoundFileData);
		}

		b = FindNextFile(hFile, &FoundFileData);

		if(!b)
			break;
	}
	_tprintf(_T("Files have finished.\n"));
	return false;
}

int _tmain(int argc, _TCHAR* argv[])
{
	ThirdProgram();
	return 0;
}

