// Lab8.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "windows.h"
#include "tlhelp32.h"
using namespace std;

#define CreateUnsuspendedProcess(commandLine, currDirectory, startupInfo, processInfo) CreateProcess(0, commandLine, 0, 0, 0, 0, 0, currDirectory, startupInfo, processInfo);
#define CreateSuspendedProcess(commandLine, currDirectory, startupInfo, processInfo) CreateProcess(0, commandLine, 0, 0, 0, CREATE_SUSPENDED, 0, currDirectory, startupInfo, processInfo);

void PrintModule(MODULEENTRY32 me)
{
	_tprintf(_T("    "));
	_tprintf(_T("%s"), me.szModule);
	_tprintf(_T("\r\n"));
}

void PrintProcessFull(PROCESSENTRY32 pi)
{
	_tprintf(_T("%d"), pi.dwSize);
	_tprintf(_T("\r\n"));
	_tprintf(_T("%d"), pi.cntUsage);
	_tprintf(_T("\r\n"));
	_tprintf(_T("%d"), pi.th32ProcessID);
	_tprintf(_T("\r\n"));
	_tprintf(_T("%d"), pi.th32DefaultHeapID);
	_tprintf(_T("\r\n"));
	_tprintf(_T("%d"), pi.th32ModuleID);
	_tprintf(_T("\r\n"));
	_tprintf(_T("%d"), pi.cntThreads);
	_tprintf(_T("\r\n"));
	_tprintf(_T("%d"), pi.th32ParentProcessID);
	_tprintf(_T("\r\n"));
	_tprintf(_T("%d"), pi.pcPriClassBase);
	_tprintf(_T("\r\n"));
	_tprintf(_T("%d"), pi.dwFlags);
	_tprintf(_T("\r\n"));
	_tprintf(_T("%s"), pi.szExeFile);
	_tprintf(_T("\r\n\r\n"));
}
void PrintProcess(PROCESSENTRY32 pi)
{
	// Header.
	_tprintf(_T("Process_#"));
	_tprintf(_T("%d"), pi.th32ProcessID);
	_tprintf(_T(" :\r\n"));
	// Name.
	_tprintf(_T("    Name:  "));
	_tprintf(_T("%s"), pi.szExeFile);
	_tprintf(_T("\r\n"));
	// Threads.
	_tprintf(_T("    Threads:  "));
	_tprintf(_T("%d"), pi.cntThreads);
	// Following indent.
	_tprintf(_T("\r\n"));
}

bool PrintProcesses(DWORD howMany)
{
	PROCESSENTRY32 pi = {sizeof(pi)};
	//MODULEENTRY32 me = {sizeof(me)};
	BOOL b;
	DWORD count = 0;
	HANDLE hSnapshot;

	hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	if(hSnapshot == INVALID_HANDLE_VALUE)
	{
		return false;
	}
	b = Process32First(hSnapshot, &pi);

	while((b) && (count < howMany)){
		PrintProcess(pi);
		//me.dwSize = sizeof(MODULEENTRY32);
		//MODULEENTRY32 me = {sizeof(me)};
		MODULEENTRY32 me;
		me.dwSize = sizeof(MODULEENTRY32);
		BOOL bModule;

		HANDLE hModule = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, pi.th32ProcessID);
		bModule = Module32First(hModule, &me);
		if(bModule)
		{
			_tprintf(_T("        Modules:\r\n"));
		}
		while(bModule)
		{
			PrintModule(me);
			bModule = Module32Next(hModule, &me);
		}
		_tprintf(_T("\r\n"));
		count++;
		b = Process32Next(hSnapshot, &pi);
		if(!b)	break;
	}
	_tprintf(_T("Total shown:  "));
	_tprintf(_T("%d"), count);
	return true;
}

bool PrintAllProcesses()
{
	PROCESSENTRY32 pi = {sizeof(pi)};
	BOOL b;
	DWORD count = 0;
	HANDLE hSnapshot;
	
	hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	if(hSnapshot == INVALID_HANDLE_VALUE)
	{
		return false;
	}
	b = Process32First(hSnapshot, &pi);
	while(b){
		PrintProcess(pi);
		MODULEENTRY32 me;
		me.dwSize = sizeof(MODULEENTRY32);
		BOOL bModule;

		HANDLE hModule = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, pi.th32ProcessID);
		bModule = Module32First(hModule, &me);
		if(bModule)
		{
			_tprintf(_T("        Modules:\r\n"));
		}
		while(bModule)
		{
			PrintModule(me);
			bModule = Module32Next(hModule, &me);
		}
		_tprintf(_T("\r\n"));
		count++;
		b = Process32Next(hSnapshot, &pi);
		if(!b)	break;
	}
	_tprintf(_T("Total shown:  "));
	_tprintf(_T("%d"), count);
	return true;
}

bool TaskManager()
{
	bool result;
	
	result = PrintAllProcesses();
	//return PrintProcesses(15);
	return result;
}

bool ThreePrograms()
{
	TCHAR commandLine[] = _T("Program_1.exe");
	TCHAR currDirectory[] = _T("d:\\notepad_files\\");
	STARTUPINFO si = {0};
	PROCESS_INFORMATION pi;	
	BOOL b;
	//SYSTEMTIME st;
	//FILETIME ft;
	
	si.cb = sizeof(STARTUPINFO);
	//GetLocalTime(&st);
	//SystemTimeToFileTime(&st, &ft);
	b = CreateUnsuspendedProcess(commandLine, currDirectory, &si, &pi);
	if(!b)
	{
		_tprintf(_T("ERROR opening Program_1 !!!\n"));
		return false;
	}
	_tprintf(_T("Program_1 process created successfully.\n"));
	WaitForSingleObject(pi.hProcess, INFINITE);
	
	// Program_1 finished.
	_tprintf(_T("Program_1 finished.\n"));
	CloseHandle(pi.hThread);
	CloseHandle(pi.hProcess);
	return true;
}

int _tmain(int argc, _TCHAR* argv[])
{
	//TaskManager();
	ThreePrograms();

	getchar();
	return 0;
}
