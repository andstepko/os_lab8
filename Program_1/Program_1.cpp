// Program_1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "windows.h"
#include "tlhelp32.h"
using namespace std;

#define CreateUnsuspendedProcess(commandLine, currDirectory, startupInfo, processInfo) CreateProcess(0, commandLine, 0, 0, 0, 0, 0, currDirectory, startupInfo, processInfo);
#define CreateSuspendedProcess(commandLine, currDirectory, startupInfo, processInfo) CreateProcess(0, commandLine, 0, 0, 0, CREATE_SUSPENDED, 0, currDirectory, startupInfo, processInfo);

bool FirstProgram()
{
	TCHAR commandLine1[] = _T("Program_2.exe");
	TCHAR commandLine2[] = _T("Program_3.exe");
	//TCHAR timeFile[] = _T("time.stepko");
	TCHAR currPath[MAX_PATH];
	TCHAR tcharTime[MAX_PATH];
	HANDLE hFile = INVALID_HANDLE_VALUE;
	DWORD dwWritten;
	STARTUPINFO si = {0};
	PROCESS_INFORMATION pi;	
	BOOL b;
	SYSTEMTIME st;
	FILETIME ft;
	
	si.cb = sizeof(STARTUPINFO);
	GetCurrentDirectory(MAX_PATH, currPath);

	GetLocalTime(&st);
	SystemTimeToFileTime(&st, &ft);

	DWORD highTime = ft.dwHighDateTime;
	DWORD lowTime = ft.dwLowDateTime;

	unsigned __int64 i64MinCreateTime = (((unsigned __int64) (ft.dwHighDateTime)) << 32) |	ft.dwLowDateTime;

	_stprintf_s(tcharTime, _T("%I64d"), i64MinCreateTime);
	SetEnvironmentVariable(_T("CreationTime"), tcharTime);
	_tprintf(_T("Environment variable was successfully created.\n"));

	_tprintf(_T("i64MinCreateTime = "));
	_tprintf(_T("%I64d"), i64MinCreateTime);
	_tprintf(_T("\n"));


	/*hFile = CreateFile((LPCTSTR)timeFile, GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ, 0, CREATE_ALWAYS, 0, 0);
	if(hFile != INVALID_HANDLE_VALUE)
	{
		_tprintf(_T("TimeFile successfully created.\n"));
	}
	else
	{
		_tprintf(_T("ERROR creating TimeFile!!!\n"));
		return false;
	}*/
	
	/*b = WriteFile(hFile, &i64MinCreateTime, sizeof(i64MinCreateTime), &dwWritten, 0);
	if(!b)
	{
		_tprintf(_T("ERROR writing into TimeFile!!!\n"));
		return false;
	}*/	

	// File was successfully created and written.
	/*_tprintf(_T("Successfully wrote: "));
	_tprintf(_T("%d"), dwWritten);
	_tprintf(_T(" bytes into timeFile.\n"));*/

	/*b = CloseHandle(hFile);
	if(b)
	{
		_tprintf(_T("TimeFile handler was successfully closed.\n"));
	}
	else
	{
		_tprintf(_T("TimeFile handler was NOT successfully closed.\n"));
	}*/

	b = CreateUnsuspendedProcess(commandLine1, currPath, &si, &pi);
	if(!b)
	{
		_tprintf(_T("ERROR opening Program_2 !!!\n"));
		return false;
	}
	_tprintf(_T("Program_2 process created successfully.\n"));

	WaitForSingleObject(pi.hProcess, INFINITE);
	
	// Program_2 finished.
	_tprintf(_T("Program_2 finished.\n"));
	CloseHandle(pi.hThread);
	CloseHandle(pi.hProcess);

	b = CreateSuspendedProcess(commandLine2, currPath, &si, &pi);
	if(!b)
	{
		_tprintf(_T("ERROR opening Program_3 !!!\n"));
		return false;
	}
	_tprintf(_T("Program_3 process created successfully.\n"));

	SetPriorityClass(pi.hProcess, BELOW_NORMAL_PRIORITY_CLASS);
	_tprintf(_T("Program_3-process priorityClass has been set to BELOW_NORMAL_PRIORITY_CLASS.\n"));

	ResumeThread(pi.hThread);
	_tprintf(_T("Program_3-thread has been resumed.\n"));

	WaitForSingleObject(pi.hProcess, INFINITE);
	
	_tprintf(_T("Program_3 finished.\n"));
	CloseHandle(pi.hThread);
	CloseHandle(pi.hProcess);

	return true;
}

int _tmain(int argc, _TCHAR* argv[])
{
	FirstProgram();

	return 0;
}

