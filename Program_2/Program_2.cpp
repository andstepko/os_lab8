// Program_2.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "windows.h"
#include "tlhelp32.h"
using namespace std;

#define CreateUnsuspendedProcess(commandLine, currDirectory, startupInfo, processInfo) CreateProcess(0, commandLine, 0, 0, 0, 0, 0, currDirectory, startupInfo, processInfo);
#define CreateSuspendedProcess(commandLine, currDirectory, startupInfo, processInfo) CreateProcess(0, commandLine, 0, 0, 0, CREATE_SUSPENDED, 0, currDirectory, startupInfo, processInfo);

bool SecondProgram()
{
	TCHAR commandLine[] = _T("notepad.exe");
	TCHAR currPath[MAX_PATH];
	STARTUPINFO si = {0};
	PROCESS_INFORMATION pi;
	BOOL b;
	
	//si.cb = sizeof(STARTUPINFO);
	GetCurrentDirectory(MAX_PATH, currPath);

	b = CreateUnsuspendedProcess(commandLine, currPath, &si, &pi);
	if(!b)
	{
		_tprintf(_T("ERROR opening Notepad !!!\n"));
		return false;
	}
	_tprintf(_T("Notepad process created successfully.\n"));
	WaitForSingleObject(pi.hProcess, INFINITE);
	
	// Notepad finished.
	_tprintf(_T("Notepad finished.\n"));
	CloseHandle(pi.hThread);
	CloseHandle(pi.hProcess);
	return true;
}

int _tmain(int argc, _TCHAR* argv[])
{
	SecondProgram();
	return 0;
}

